<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\ProductsRequest;
use App\Http\Resources\ProductResource;
use App\Http\Resources\ProductCollection;
use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    protected $product;

    public function __construct(Product $product)
    {
        $this->product = $product;
    }
    public function index()
    {
        $products = $this->product->paginate(5);
        $ProductsCollection = new ProductCollection($products);
        return $this->successResponse($ProductsCollection, 'Danh sách sản phẩm!');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ProductsRequest $request)
    {
        $createProduct = $request->all();
        $product = $this->product->create($createProduct);
        $ProductResource = new ProductResource($product);
        return $this->successResponse($ProductResource, 'Thêm thành công!');
    }


    public function show($id)
    {
        $product = $this->product->findOrFail($id);
        $ProductResource = new ProductResource($product);
        return $this->successResponse($ProductResource, 'Yêu cầu thành công!');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     */
    public function update(Request $request, $id)
    {
        $updateProduct = $request->all();
        $product = $this->product->findOrFail($id);
        $product->update($updateProduct);
        $ProductResource = new ProductResource($product);
        return $this->successResponse($ProductResource, 'Cập nhật thành công!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     */
    public function destroy($id)
    {
        $product = $this->product->findOrFail($id);
        $product->delete();
        $ProductResource = new ProductResource($product);
        return $this->successResponse($ProductResource, 'Product Deleted');
    }
}
